#!/bin/bash

python search.py --dataset cifar10 --data_loc '../nas-bench-datasets/cifar10'            --n_runs $1 --n_samples 10 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset cifar10 --trainval --data_loc '../nas-bench-datasets/cifar10' --n_runs $1 --n_samples 10 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset cifar100 --data_loc '../nas-bench-datasets/cifar100'          --n_runs $1 --n_samples 10 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset ImageNet16-120 --data_loc '../nas-bench-datasets/ImageNet16'  --n_runs $1 --n_samples 10 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'

python search.py --dataset cifar10 --data_loc '../nas-bench-datasets/cifar10'            --n_runs $1 --n_samples 100 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset cifar10 --trainval --data_loc '../nas-bench-datasets/cifar10' --n_runs $1 --n_samples 100 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset cifar100 --data_loc '../nas-bench-datasets/cifar100'          --n_runs $1 --n_samples 100 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset ImageNet16-120 --data_loc '../nas-bench-datasets/ImageNet16'  --n_runs $1 --n_samples 100 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'

python search.py --dataset cifar10 --data_loc '../nas-bench-datasets/cifar10'            --n_runs $1 --n_samples 500 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset cifar10 --trainval --data_loc '../nas-bench-datasets/cifar10' --n_runs $1 --n_samples 500 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset cifar100 --data_loc '../nas-bench-datasets/cifar100'          --n_runs $1 --n_samples 500 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset ImageNet16-120 --data_loc '../nas-bench-datasets/ImageNet16'  --n_runs $1 --n_samples 500 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'

python search.py --dataset cifar10 --data_loc '../nas-bench-datasets/cifar10'            --n_runs $1 --n_samples 1000 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset cifar10 --trainval --data_loc '../nas-bench-datasets/cifar10' --n_runs $1 --n_samples 1000 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset cifar100 --data_loc '../nas-bench-datasets/cifar100'          --n_runs $1 --n_samples 1000 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'
python search.py --dataset ImageNet16-120 --data_loc '../nas-bench-datasets/ImageNet16'  --n_runs $1 --n_samples 1000 --api_loc '../nas-bench-datasets/NATS-tss-v1_0-3ffb9-simple/'

python process_results.py --n_runs $1