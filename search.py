import os
import time
import argparse
import random
import numpy as np

from tqdm import trange
from statistics import mean

import torch

import xautodl
from xautodl.models import get_cell_based_tiny_net
from xautodl.utils import count_parameters_in_MB

from datasets import get_datasets
from config_utils import load_config

from nats_bench import create

parser = argparse.ArgumentParser(description="NAS Without Training")
parser.add_argument("--data_loc",
                    default="./datasets/cifar10",
                    type=str,
                    help="dataset folder")
parser.add_argument(
    "--api_loc",
    default="./datasets/NATS-sss-v1_0-50262-simple/",
    type=str,
    help="path to API",
)
parser.add_argument("--save_loc",
                    default="results",
                    type=str,
                    help="folder to save results")
parser.add_argument("--batch_size", default=256, type=int)
parser.add_argument("--evaluate_size", default=256, type=int)
parser.add_argument("--GPU", default="0", type=str)
parser.add_argument("--seed", default=1, type=int)
parser.add_argument("--trainval", action="store_true")
parser.add_argument("--dataset", default="cifar10", type=str)
parser.add_argument("--n_samples", default=100, type=int)
parser.add_argument("--n_runs", default=1, type=int)

args = parser.parse_args()
os.environ["CUDA_VISIBLE_DEVICES"] = args.GPU

# Reproducibility
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
random.seed(args.seed)
np.random.seed(args.seed)
torch.manual_seed(args.seed)


def get_batch_jacobian(net, x, target):
  net.zero_grad()
  x.requires_grad_(True)
  _, y = net(x)
  y.backward(torch.ones_like(y))
  jacob = x.grad.detach()
  return jacob, target.detach()


def eval_score_perclass(jacob, labels=None, n_classes=10):
  k = 1e-5
  per_class = {}
  for i, label in enumerate(labels[0]):
    if label in per_class:
      per_class[label] = np.vstack((per_class[label], jacob[i]))
    else:
      per_class[label] = jacob[i]

  ind_corr_matrix_score = {}
  for c in per_class.keys():
    s = 0
    try:
      corrs = np.corrcoef(per_class[c])

      s = np.sum(np.log(abs(corrs) + k))
      if n_classes > 100:
        s /= len(corrs)
    except:
      continue

    ind_corr_matrix_score[c] = s

  # per class-corr matrix A and B
  score = 0
  ind_corr_matrix_score_keys = ind_corr_matrix_score.keys()
  if n_classes <= 100:

    for c in ind_corr_matrix_score_keys:
      # B)
      score += np.absolute(ind_corr_matrix_score[c])
  else:
    for c in ind_corr_matrix_score_keys:
      # A)
      for cj in ind_corr_matrix_score_keys:
        score += np.absolute(ind_corr_matrix_score[c] -
                             ind_corr_matrix_score[cj])

    # should divide by number of classes seen
    score /= len(ind_corr_matrix_score_keys)

  return score


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
THE_START = time.time()
api = create(args.api_loc, "sss", fast_mode=True, verbose=False)

os.makedirs(args.save_loc, exist_ok=True)

train_data, valid_data, xshape, class_num = get_datasets(args.dataset,
                                                         args.data_loc,
                                                         cutout=0)

if args.dataset == "cifar10":
  acc_type = "ori-test"
  val_acc_type = "x-valid"

else:
  acc_type = "x-test"
  val_acc_type = "x-valid"

if args.trainval:
  cifar_split = load_config("config_utils/cifar-split.txt", None, None)
  train_split, valid_split = cifar_split.train, cifar_split.valid
  train_loader = torch.utils.data.DataLoader(
      train_data,
      batch_size=args.batch_size,
      num_workers=0,
      pin_memory=True,
      sampler=torch.utils.data.sampler.SubsetRandomSampler(train_split),
  )

else:
  train_loader = torch.utils.data.DataLoader(
      train_data,
      batch_size=args.batch_size,
      shuffle=True,
      num_workers=0,
      pin_memory=True,
  )

times = []
chosen = []
acc = []
val_acc = []
topscores = []
top_sizes_in_mb = []

dset = args.dataset if not args.trainval else "cifar10-valid"

order_fn = np.nanargmax

runs = trange(args.n_runs, desc="")

# Main process
for N in runs:
  start = time.time()
  indices = np.random.randint(0, len(api), args.n_samples)
  scores = []
  sizes_in_mb = []
  sizes_acc = []
  for arch in indices:
    data_iterator = iter(train_loader)
    x, target = next(data_iterator)
    x, target = x.to(device), target.to(device)

    config = api.get_net_config(arch, args.dataset)

    network = get_cell_based_tiny_net(config)
    network = network.to(device)

    info_size = api.get_more_info(int(arch), args.dataset)["train-accuracy"]

    jacobs = []
    targets = []
    grads = []
    iterations = np.int(np.ceil(args.evaluate_size / args.batch_size))

    for i in range(iterations):
      jacobs_batch, target = get_batch_jacobian(network, x, target)
      jacobs.append(
          jacobs_batch.reshape(jacobs_batch.size(0), -1).cpu().numpy())
      targets.append(target.cpu().numpy())
    jacobs = np.concatenate(jacobs, axis=0)
    if jacobs.shape[0] > args.evaluate_size:
      jacobs = jacobs[0:args.evaluate_size, :]

    try:
      s = eval_score_perclass(jacobs, targets)

    except Exception as e:
      print(e)
      s = np.nan
    scores.append(s)
    # print("score:", s)
    sizes_in_mb.append(count_parameters_in_MB(network))
    # print("sizes_in_mb:", count_parameters_in_MB(network))
    sizes_acc.append(info_size)
    # print("size_acc:", info_size)

  best_arch = indices[order_fn(scores)]
  info = api.query_by_index(best_arch)
  topscores.append(scores[order_fn(scores)])
  top_sizes_in_mb.append(sizes_in_mb[order_fn(sizes_in_mb)])
  chosen.append(best_arch)
  acc.append(info.get_metrics(dset, acc_type)["accuracy"])

  if not args.dataset == "cifar10" or args.trainval:
    val_acc.append(info.get_metrics(dset, val_acc_type)["accuracy"])

  times.append(time.time() - start)
  runs.set_description(
      f"acc: {mean(acc if not args.trainval else val_acc):.2f}%")

  print(times)

dset = args.dataset if not args.trainval else "cifar10-valid"

fname = f"{args.save_loc}/{dset}_{args.n_runs}_{args.n_samples}_{args.seed}_scores"
accfilename = f"{args.save_loc}/{dset}_{args.n_runs}_{args.n_samples}_{args.seed}_acc"
sizefname = f"{args.save_loc}/{dset}_{args.n_runs}_{args.n_samples}_{args.seed}_size"

np.save(fname, scores)
np.save(accfilename, sizes_acc)
np.save(sizefname, sizes_in_mb)